#include "break/json/Json.h"

#include <mn/Pool.h>
#include <mn/Str_Intern.h>

#include <string.h>

namespace brk::json
{
	using namespace mn;

	struct Internal_Json
	{
		Allocator	arena;
		Str_Intern	str_table;
		Value*	root;
		Value* null;
	};

	inline static Value*
	_json_value_get(Internal_Json* self)
	{
		Value* value = alloc_from<Value>(self->arena);
		value->doc = (Doc)self;
		return value;
	}

	inline static Array*
	_json_array_get(Internal_Json* self)
	{
		Array* array = alloc_from<Array>(self->arena);
		*array = buf_with_allocator<Value*>(self->arena);
		return array;
	}

	inline static Object*
	_json_object_get(Internal_Json* self)
	{
		Object* object = alloc_from<Object>(self->arena);
		*object = map_with_allocator<const char*, Value*>(self->arena);
		return object;
	}

	Doc
	doc_new()
	{
		Internal_Json* self = alloc<Internal_Json>();
		self->arena = allocator_arena_new();
		self->str_table = str_intern_new();
		self->null = _json_value_get(self);
		self->null->kind = Value::KIND_NULL;
		return (Doc)self;
	}

	void
	doc_free(Doc json)
	{
		Internal_Json* self = (Internal_Json*)json;
		allocator_free(self->arena);
		str_intern_free(self->str_table);
		free(self);
	}

	Value*
	doc_root(Doc json)
	{
		Internal_Json* self = (Internal_Json*)json;
		return self->root;
	}

	void
	doc_root(Doc json, Value* root)
	{
		Internal_Json* self = (Internal_Json*)json;
		self->root = root;
	}

	const char*
	doc_intern(Doc json, const Str& str)
	{
		Internal_Json* self = (Internal_Json*)json;
		return str_intern(self->str_table, str);
	}

	Value*
	doc_null(Doc json)
	{
		Internal_Json* self = (Internal_Json*)json;
		return self->null;
	}

	Value*
	doc_bool(Doc json, bool val_bool)
	{
		Internal_Json* self = (Internal_Json*)json;
		Value* value = _json_value_get(self);
		value->kind = Value::KIND_BOOL;
		value->val_bool = val_bool;
		return value;
	}

	Value*
	doc_num(Doc json, double val_num)
	{
		Internal_Json* self = (Internal_Json*)json;
		Value* value = _json_value_get(self);
		value->kind = Value::KIND_NUMBER;
		value->val_num = val_num;
		return value;
	}

	Value*
	doc_str(Doc json, const mn::Str& val_str)
	{
		Internal_Json* self = (Internal_Json*)json;
		Value* value = _json_value_get(self);
		value->kind = Value::KIND_STRING;
		value->val_str = str_intern(self->str_table, val_str);
		return value;
	}

	Value*
	doc_arr(Doc json, const std::initializer_list<Value*>& elements)
	{
		Internal_Json* self = (Internal_Json*)json;
		Value* value = _json_value_get(self);
		value->kind = Value::KIND_ARRAY;
		value->val_arr = _json_array_get(self);

		Array& buf = *value->val_arr;
		buf_resize(buf, elements.size());

		size_t i = 0;
		for(Value* element: elements)
		{
			assert(element->doc == json &&
				"you can't have json values from another json object be array elements of this json object");
			buf[i] = element;
			++i;
		}

		return value;
	}

	Value*
	doc_obj(Doc json, const std::initializer_list<Member>& members)
	{
		Internal_Json* self = (Internal_Json*)json;
		Value* value = _json_value_get(self);
		value->kind = Value::KIND_OBJECT;
		value->val_obj = _json_object_get(self);

		Object& map = *value->val_obj;
		map_reserve(map, members.size());

		for(const Member& member: members)
		{
			assert(member.value->doc == json &&
				"you can't have json values from another json object be object members of this json object");
			map_insert(map, str_intern(self->str_table, member.key), member.value);
		}

		return value;
	}

	//json parsing

	//Json_Location
	struct Location
	{
		uint32_t line, column;
	};

	//Json_Token
	struct Token
	{
		enum KIND
		{
			KIND_NONE,
			KIND_OPEN_CURLY,
			KIND_CLOSE_CURLY,
			KIND_OPEN_BRACKET,
			KIND_CLOSE_BRACKET,
			KIND_COMMA,
			KIND_COLON,
			KIND_BOOL,
			KIND_NUMBER,
			KIND_STRING,
			KIND_NULL
		};

		inline operator bool() const { return kind != KIND_NONE; }

		KIND kind;
		const char* str;
		Location loc;
		const char* begin, *end;
		union
		{
			bool val_bool;
			double val_number;
		};
	};

	inline static const char*
	token_kind_str(Token::KIND kind)
	{
		switch(kind)
		{
			case Token::KIND_OPEN_CURLY:	return "{";
			case Token::KIND_CLOSE_CURLY:	return "}";
			case Token::KIND_OPEN_BRACKET:	return "[";
			case Token::KIND_CLOSE_BRACKET: return "]";
			case Token::KIND_COMMA:			return ",";
			case Token::KIND_COLON:			return ":";
			case Token::KIND_BOOL:			return "bool";
			case Token::KIND_NUMBER:		return "number";
			case Token::KIND_STRING:		return "string";
			case Token::KIND_NULL:			return "null";

			case Token::KIND_NONE:
			default:
				return "unidentified";
		}
	}

	//Doc Lexer
	struct Lexer
	{
		//iteration and the current character
		const char* it;
		int32_t c;

		//location of the current character
		Location loc;

		//current lexed token
		Token tkn;

		//intern table
		Str_Intern* str_table;

		//keywords
		const char* keyword_true;
		const char* keyword_false;
		const char* keyword_null;

		//buffer for token peeking
		Buf<Token> buffer;
		size_t buffer_ix;
	};

	bool
	lexer_eof(Lexer& self)
	{
		return self.c == 0;
	}

	inline static Lexer
	lexer_new(const mn::Str& content, Str_Intern* table)
	{
		Lexer self{};
		self.it = content.ptr;
		self.c = rune_read(self.it);
		self.loc.line = 1;
		self.str_table = table;
		self.keyword_null = str_intern(*table, "null");
		self.keyword_true = str_intern(*table, "true");
		self.keyword_false = str_intern(*table, "false");
		self.buffer = buf_new<Token>();
		return self;
	}

	inline static void
	lexer_free(Lexer& self)
	{
		buf_free(self.buffer);
	}

	inline static bool
	lexer_is_ws(int32_t c)
	{
		return (c == ' '  ||
				c == '\t' ||
				c == '\n' ||
				c == '\r');
	}

	inline static bool
	lexer_read_rune(Lexer& self)
	{
		if(lexer_eof(self))
			return false;

		self.it = rune_next(self.it);
		self.c = rune_read(self.it);

		if(self.c == '\n')
		{
			self.loc.column = 0;
			++self.loc.line;
		}
		else
		{
			++self.loc.column;
		}
		return true;
	}

	inline static void
	lexer_skip_ws(Lexer& self)
	{
		while(lexer_is_ws(self.c))
			if(lexer_read_rune(self) == false)
				break;
	}

	inline static bool
	lexer_is_letter(int32_t c)
	{
		if(c < 0x80)
			return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
		return false;
	}

	inline static bool
	lexer_is_digit(int32_t c)
	{
		return (c >= '0' && c <= '9');
	}

	inline static const char*
	lexer_scan_id(Lexer& self)
	{
		auto old_it = self.it;
		while(lexer_is_letter(self.c))
			if(lexer_read_rune(self) == false)
				break;
		self.tkn.begin = old_it;
		self.tkn.end = self.it;
		return str_intern(*self.str_table, self.tkn.begin, self.tkn.end);
	}

	inline static const char*
	lexer_scan_str(Lexer& self)
	{
		const char* old_it = self.it;
		int32_t prev = '"';
		bool ignore = false;
		while(lexer_read_rune(self))
		{
			if(prev == '\\' && !ignore)
			{
				ignore = true;
			}
			else if(self.c == '"' && !ignore)
			{
				break;
			}
			else
			{
				ignore = false;
			}

			prev = self.c;
		}

		self.tkn.begin = old_it;
		self.tkn.end = self.it;
		lexer_read_rune(self);
		return str_intern(*self.str_table, self.tkn.begin, self.tkn.end);
	}

	inline static Token
	lexer_lex(Lexer& self)
	{
		//skip whitespaces first
		lexer_skip_ws(self);

		if(lexer_eof(self))
		{
			self.tkn = Token{};
			return self.tkn;
		}

		self.tkn = Token{};
		self.tkn.begin = self.it;
		self.tkn.loc = self.loc;

		if(lexer_is_letter(self.c))
		{
			self.tkn.str = lexer_scan_id(self);
			if(self.tkn.str == self.keyword_null)
			{
				self.tkn.kind = Token::KIND_NULL;
			}
			else if(self.tkn.str == self.keyword_true)
			{
				self.tkn.kind = Token::KIND_BOOL;
				self.tkn.val_bool = true;
			}
			else if(self.tkn.str == self.keyword_false)
			{
				self.tkn.kind = Token::KIND_BOOL;
				self.tkn.val_bool = false;
			}
		}
		else if(lexer_is_digit(self.c) || self.c == '-' || self.c == '+')
		{
			char* end = nullptr;

			self.tkn.kind = Token::KIND_NUMBER;
			self.tkn.val_number = ::strtod(self.it, &end);
			self.tkn.str = str_intern(*self.str_table, self.it, end);

			self.it = end;
			self.c = rune_read(self.it);
		}
		else
		{
			int32_t c = self.c;
			lexer_read_rune(self);

			switch(c)
			{
				case '"':
					self.tkn.kind = Token::KIND_STRING;
					self.tkn.str = lexer_scan_str(self);
					break;

				case ':':
					self.tkn.kind = Token::KIND_COLON;
					self.tkn.str = str_intern(*self.str_table, ":");
					break;

				case ',':
					self.tkn.kind = Token::KIND_COMMA;
					self.tkn.str = str_intern(*self.str_table, ",");
					break;

				case '{':
					self.tkn.kind = Token::KIND_OPEN_CURLY;
					self.tkn.str = str_intern(*self.str_table, "{");
					break;

				case '}':
					self.tkn.kind = Token::KIND_CLOSE_CURLY;
					self.tkn.str = str_intern(*self.str_table, "}");
					break;

				case '[':
					self.tkn.kind = Token::KIND_OPEN_BRACKET;
					self.tkn.str = str_intern(*self.str_table, "[");
					break;

				case ']':
					self.tkn.kind = Token::KIND_CLOSE_BRACKET;
					self.tkn.str = str_intern(*self.str_table, "]");
					break;

				default:
					break;
			}
		}

		self.tkn.end = self.it;
		return self.tkn;
	}

	Token
	lexer_peek(Lexer& self)
	{
		if(self.buffer.count > self.buffer_ix)
		{
			Token tkn = self.buffer[self.buffer_ix];
			self.loc = tkn.loc;
			return tkn;
		}

		auto tkn = lexer_lex(self);
		if(tkn)
			buf_push(self.buffer, tkn);
		return tkn;
	}

	Token
	lexer_consume(Lexer& self)
	{
		if(self.buffer.count > self.buffer_ix)
		{
			Token tkn = self.buffer[self.buffer_ix++];
			self.loc = tkn.loc;
			return tkn;
		}

		//nothing buffered so clear the array
		buf_clear(self.buffer);
		self.buffer_ix = 0;
		return lexer_lex(self);
	}


	struct Parser
	{
		Doc json;
		Lexer* lexer;
		Str* errors;
		size_t errors_count;
	};

	inline static Parser
	parser_new(Doc json, Lexer* lexer, Str* errors)
	{
		Parser self{};
		self.json = json;
		self.lexer = lexer;
		self.errors = errors;
		return self;
	}

	inline static Token
	parser_peek(Parser& self)
	{
		return lexer_peek(*self.lexer);
	}

	inline static Token
	parser_peek(Parser& self, Token::KIND kind)
	{
		auto tkn = parser_peek(self);
		if(tkn.kind == kind)
			return tkn;
		return Token{};
	}

	inline static Token
	parser_peek_must(Parser& self, Token::KIND kind)
	{
		auto tkn = parser_peek(self);
		if(tkn.kind != kind)
		{
			if(self.errors)
			{
				str_pushf(*self.errors, "[Error: %zu, %zu]: expected '%s' but found '%s'\n",
						  self.lexer->loc.line, self.lexer->loc.column,
						  token_kind_str(kind), tkn.str);
			}
			++self.errors_count;
			return Token{};
		}
		return tkn;
	}

	inline static Token
	parser_consume(Parser& self)
	{
		return lexer_consume(*self.lexer);
	}

	inline static Token
	parser_consume(Parser& self, Token::KIND kind)
	{
		auto tkn = parser_peek(self);
		if(tkn.kind == kind)
			return parser_consume(self);
		return Token{};
	}

	inline static Token
	parser_consume_must(Parser& self, Token::KIND kind)
	{
		auto tkn = parser_peek(self);
		if(tkn.kind != kind)
		{
			if(self.errors)
			{
				str_pushf(*self.errors, "[Error: %zu, %zu]: expected '%s' but found '%s'",
						  self.lexer->loc.line, self.lexer->loc.column,
						  token_kind_str(kind), tkn.str);
			}
			++self.errors_count;
			return Token{};
		}
		return parser_consume(self);
	}


	//Doc Parser
	inline static Value*
	parser_parse_value(Parser& self)
	{
		if(Token tkn = parser_consume(self, Token::KIND_NULL))
		{
			return doc_null(self.json);
		}
		else if(Token tkn = parser_consume(self, Token::KIND_BOOL))
		{
			return doc_bool(self.json, tkn.val_bool);
		}
		else if(Token tkn = parser_consume(self, Token::KIND_NUMBER))
		{
			return doc_num(self.json, tkn.val_number);
		}
		else if(Token tkn = parser_consume(self, Token::KIND_STRING))
		{
			return doc_str(self.json, tkn.str);
		}
		else if(Token tkn = parser_consume(self, Token::KIND_OPEN_BRACKET))
		{
			Value* array = doc_arr(self.json);
			while(parser_peek(self, Token::KIND_CLOSE_BRACKET) == false)
			{
				if(Value* value = parser_parse_value(self))
					arr_push(array, value);

				if(parser_consume(self, Token::KIND_COMMA) == false)
					break;
			}
			parser_consume_must(self, Token::KIND_CLOSE_BRACKET);
			return array;
		}
		else if(Token tkn = parser_consume(self, Token::KIND_OPEN_CURLY))
		{
			Value* object = doc_obj(self.json);

			while(parser_peek(self, Token::KIND_CLOSE_CURLY) == false)
			{
				Token key = parser_consume_must(self, Token::KIND_STRING);
				parser_consume_must(self, Token::KIND_COLON);
				Value* value = parser_parse_value(self);

				obj_add(object, key.str, value);
				if(parser_consume(self, Token::KIND_COMMA) == false)
					break;
			}
			parser_consume_must(self, Token::KIND_CLOSE_CURLY);
			return object;
		}
		else if(Token tkn = parser_consume(self))
		{
			str_pushf(*self.errors, "[Error: %zu, %zu]: unidentified token '%s' of kind '%s'",
					  self.lexer->loc.line, self.lexer->loc.column,
					  tkn.str, token_kind_str(tkn.kind));
		}
	}

	Doc
	parse(const mn::Str& content, mn::Str* errors)
	{
		Doc result = doc_new();
		Internal_Json* self = (Internal_Json*)result;

		Lexer lexer = lexer_new(content, &self->str_table);
		Parser parser = parser_new(result, &lexer, errors);

		self->root = parser_parse_value(parser);

		if(parser.errors_count)
		{
			doc_free(result);
			return nullptr;
		}

		lexer_free(lexer);

		return result;
	}
}
