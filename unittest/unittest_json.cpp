#include <catch2/catch.hpp>

#include <break/json/Json.h>

using namespace brk;

TEST_CASE("json creation", "[ms-json]")
{
	using namespace brk::json;

	Doc doc = doc_new();
	Value* obj = doc_obj(doc, {
	   {"name",		doc_str(doc, "Mostafa")},
	   {"age",		doc_num(doc, 25)},
	   {"cool",		doc_bool(doc, true)},
	   {"phones",	doc_arr(doc, {
		   doc_str(doc, "0123456789"),
		   doc_str(doc, "123456879")
		})}
	});

	CHECK(obj_of(obj, "Name") == nullptr);
	CHECK(obj_of(obj, "name") != nullptr);
	CHECK(::strcmp(as_str(obj_of(obj, "name")), "Mostafa") == 0);
	CHECK(as_num(obj_of(obj, "age")) == 25);
	CHECK(as_bool(obj_of(obj, "cool")) == true);
	CHECK(::strcmp(as_str(arr_of(obj_of(obj, "phones"), 0)), "0123456789") == 0);
	CHECK(::strcmp(as_str(arr_of(obj_of(obj, "phones"), 1)), "123456879") == 0);

	doc_free(doc);
}

TEST_CASE("json parsing", "[ms-json]")
{
	using namespace brk::json;

	const char* str = R"JSON(
	{
		"name": "Mostafa",
		"age": 25,
		"cool": true,
		"empty": null,
		"phones": ["0123456789", 123456789]
	}
	)JSON";
	Doc doc = parse(str, nullptr);
	Value* obj = doc_root(doc);

	CHECK(obj_of(obj, "Name") == nullptr);
	CHECK(obj_of(obj, "name") != nullptr);
	CHECK(::strcmp(as_str(obj_of(obj, "name")), "Mostafa") == 0);
	CHECK(as_num(obj_of(obj, "age")) == 25);
	CHECK(as_bool(obj_of(obj, "cool")) == true);
	CHECK(::strcmp(as_str(arr_of(obj_of(obj, "phones"), 0)), "0123456789") == 0);
	CHECK(as_num(arr_of(obj_of(obj, "phones"), 1)) == 123456789);
	CHECK(obj_of(obj, "empty") == doc_null(doc));

	doc_free(doc);
}

