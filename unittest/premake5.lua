project "unittest"
	language "C++"
	kind "ConsoleApp"

	files
	{
		"unittest_main.cpp",
		"unittest_json.cpp"
	}

	includedirs
	{
		"Catch2/single_include",
		"%{mn}/include",
		"../include"
	}

	links
	{
		"mn",
		"json"
	}

	cppdialect "c++17"
	systemversion "latest"

	filter "system:linux"
		defines { "OS_LINUX" }

	filter "system:windows"
		defines { "OS_WINDOWS" }