#pragma once

#if defined(OS_WINDOWS)
	#if defined(JSON_DLL)
		#define API_JSON __declspec(dllexport)
	#else
		#define API_JSON __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_JSON 
#endif