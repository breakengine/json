#pragma once

#include "break/json/Exports.h"

#include <mn/Map.h>
#include <mn/Str.h>
#include <mn/Stream.h>
#include <mn/IO.h>

namespace brk::json
{
	MS_FWD_HANDLE(Doc);

	//Value
	struct Value;

	using Array = mn::Buf<Value*>;
	using Object = mn::Map<const char*, Value*>;

	struct Value
	{
		enum KIND
		{
			KIND_NONE,
			KIND_NULL,
			KIND_BOOL,
			KIND_NUMBER,
			KIND_STRING,
			KIND_ARRAY,
			KIND_OBJECT
		};

		KIND kind;
		Doc doc;
		union
		{
			bool		val_bool;

			double		val_num;

			const char* val_str;

			Array* val_arr;

			Object* val_obj;
		};
	};


	//This is just a helper struct for the object initialization
	struct Member
	{
		mn::Str key;
		Value* value;

		Member(const char* _key, Value* _value)
			:key(mn::str_lit(_key)), value(_value)
		{}

		Member(const mn::Str& _key, Value* _value)
			:key(_key), value(_value)
		{}
	};


	//Doc
	MS_HANDLE(Doc);

	API_JSON Doc
	doc_new();

	API_JSON void
	doc_free(Doc json);

	inline static void
	destruct(Doc& self)
	{
		doc_free(self);
	}

	API_JSON Value*
	doc_root(Doc json);

	API_JSON void
	doc_root(Doc json, Value* root);

	API_JSON const char*
	doc_intern(Doc json, const mn::Str& str);

	inline static const char*
	doc_intern(Doc json, const char* str)
	{
		return doc_intern(json, mn::str_lit(str));
	}

	API_JSON Value*
	doc_null(Doc json);

	API_JSON Value*
	doc_bool(Doc json, bool val_bool);

	API_JSON Value*
	doc_num(Doc json, double val_num);

	API_JSON Value*
	doc_str(Doc json, const mn::Str& val_str);

	inline static Value*
	doc_str(Doc json, const char* val_str)
	{
		return doc_str(json, mn::str_lit(val_str));
	}

	API_JSON Value*
	doc_arr(Doc json, const std::initializer_list<Value*>& elements = {});

	API_JSON Value*
	doc_obj(Doc json, const std::initializer_list<Member>& members = {});

	inline static bool&
	as_bool(Value* self)
	{
		assert(self->kind == Value::KIND_BOOL);
		return self->val_bool;
	}

	inline static const bool&
	as_bool(const Value* self)
	{
		assert(self->kind == Value::KIND_BOOL);
		return self->val_bool;
	}

	inline static double&
	as_num(Value* self)
	{
		assert(self->kind == Value::KIND_NUMBER);
		return self->val_num;
	}

	inline static const double&
	as_num(const Value* self)
	{
		assert(self->kind == Value::KIND_NUMBER);
		return self->val_num;
	}

	inline static const char*
	as_str(const Value* self)
	{
		assert(self->kind == Value::KIND_STRING);
		return self->val_str;
	}

	inline static Value*
	arr_of(Value* self, size_t ix)
	{
		assert(self->kind == Value::KIND_ARRAY);
		return (*self->val_arr)[ix];
	}

	inline static const Value*
	arr_of(const Value* self, size_t ix)
	{
		assert(self->kind == Value::KIND_ARRAY);
		return (*self->val_arr)[ix];
	}

	inline static Value*
	obj_of(Value* self, const char* key)
	{
		assert(self->kind == Value::KIND_OBJECT);
		if(auto it = mn::map_lookup(*self->val_obj, doc_intern(self->doc, key)))
			return it->value;
		return nullptr;
	}

	inline static Value*
	obj_of(Value* self, const mn::Str& key)
	{
		assert(self->kind == Value::KIND_OBJECT);
		if(auto it = mn::map_lookup(*self->val_obj, doc_intern(self->doc, key)))
			return it->value;
		return nullptr;
	}

	inline static const Value*
	obj_of(const Value* self, const char* key)
	{
		assert(self->kind == Value::KIND_OBJECT);
		if(auto it = mn::map_lookup(*self->val_obj, doc_intern(self->doc, key)))
			return it->value;
		return nullptr;
	}

	inline static const Value*
	obj_of(const Value* self, const mn::Str& key)
	{
		assert(self->kind == Value::KIND_OBJECT);
		if(auto it = mn::map_lookup(*self->val_obj, doc_intern(self->doc, key)))
			return it->value;
		return nullptr;
	}

	inline static void
	arr_push(Value* self, Value* value)
	{
		assert(self->kind == Value::KIND_ARRAY && "Not a valid array");
		assert(value->doc == self->doc &&
			   "you can't have json values from another json object be array elements of this json object");
		buf_push(*self->val_arr, value);
	}

	inline static void
	obj_add(Value* self, const char* key, Value* value)
	{
		assert(self->kind == Value::KIND_OBJECT && "Not a valid object");
		assert(value->doc == self->doc &&
			   "you can't have json values from another json object be object members of this json object");
		map_insert(*self->val_obj, doc_intern(self->doc, key), value);
	}

	inline static void
	obj_add(Value* self, const mn::Str& key, Value* value)
	{
		assert(self->kind == Value::KIND_OBJECT && "Not a valid object");
		assert(value->doc == self->doc &&
			   "you can't have json values from another json object be object members of this json object");
		map_insert(*value->val_obj, doc_intern(self->doc, key), value);
	}

	inline static void
	obj_remove(Value* self, const char* key)
	{
		assert(self->kind == Value::KIND_OBJECT && "Not a valid object");
		map_remove(*self->val_obj, doc_intern(self->doc, key));
	}

	inline static void
	obj_remove(Value* self, const mn::Str& key)
	{
		assert(self->kind == Value::KIND_OBJECT && "Not a valid object");
		map_remove(*self->val_obj, doc_intern(self->doc, key));
	}

	inline static void
	dump(Value* value, mn::Stream out = mn::stream_stdout())
	{
		switch(value->kind)
		{
			case Value::KIND_NULL:
				mn::vprintf(out, "null");
				break;

			case Value::KIND_BOOL:
				mn::vprintf(out, "{}", value->val_bool ? "true" : "false");
				break;

			case Value::KIND_NUMBER:
				mn::vprintf(out, "{}", value->val_num);
				break;

			case Value::KIND_STRING:
				mn::vprintf(out, "\"{}\"", value->val_str);
				break;

			case Value::KIND_ARRAY:
			{
				mn::vprintf(out, "[");
				for(size_t i = 0; i < value->val_arr->count; ++i)
				{
					if(i != 0)
						mn::vprintf(out, ", ");
					dump((*value->val_arr)[i], out);
				}
				mn::vprintf(out, "]");
				break;
			}

			case Value::KIND_OBJECT:
			{
				mn::vprintf(out, "{");
				auto& members = *value->val_obj;
				size_t i = 0;
				for(auto it = map_begin(members);
					it != map_end(members);
					it = map_next(members, it))
				{
					if(i != 0)
						mn::vprintf(out, ", ");
					mn::vprintf(out, "\"{}\": ", it->key);
					dump(it->value, out);
					++i;
				}
				mn::vprintf(out, "}");
				break;
			}

			default:
				assert(false && "Invalid Value");
				break;
		}
	}

	API_JSON Doc
	parse(const mn::Str& content, mn::Str* errors);

	inline static Doc
	parse(const char* content, mn::Str* errors)
	{
		return parse(mn::str_lit(content), errors);
	}
}
